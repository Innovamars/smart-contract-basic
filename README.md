# smart-cntract-basic
const Web3 = require("web3");

const DEFAULT_GAS_WEI = 4600000;
const DEFAULT_ADDRESS_COUNT = 3;
const DEFAULT_ADDRESS_INDEX = 0;
const DEFAULT_GAS_GWEI_PRICE = "20";

const web3 = new Web3();
const HDWalletProvider = require("truffle-hdwallet-provider");

const addressCountValue = process.env["ADDRESS_COUNT_KEY"] || DEFAULT_ADDRESS_COUNT;
const mnemonicKeyValue = process.env["MNEMONIC_KEY"] || '';
const infuraKeyValue = process.env["INFURA_KEY"] || '';

if(infuraKeyValue === '' || mnemonicKeyValue === '') {
  console.log('WARNING: The infura key or/and mnemonic key are empty. They should not be empty.');
}

const gasKeyValue = process.env["GAS_WEI_KEY"] || DEFAULT_GAS_WEI;
const gasPriceKeyValue = process.env["GAS_PRICE_GWEI_KEY"] || DEFAULT_GAS_GWEI_PRICE;

    // By default WebpackDevServer serves physical files from current directory
    // in addition to all the virtual build products that it serves from memory.
    // This is confusing because those files won’t automatically be available in
    // production build folder unless we copy them. However, copying the whole
    // project directory is dangerous because we may expose sensitive files.
    // Instead, we establish a convention that only files in `public` directory
    // get served. Our build script will copy `public` into the `build` folder.
    // In `index.html`, you can get URL of `public` folder with %PUBLIC_PATH%:
    // <link rel="shortcut icon" href="%PUBLIC_URL%/favicon.ico">
    // In JavaScript code, you can access it with `process.env.PUBLIC_URL`.
    // Note that we only recommend to use `public` folder as an escape hatch
    // for files like `favicon.ico`, `manifest.json`, and libraries that are
    // for some reason broken when imported through Webpack. If you just want to
    // use an image, put it in `src` and `import` it from JavaScript instead.
